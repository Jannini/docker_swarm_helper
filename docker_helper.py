import docker
import paramiko
import sys
import math
import os
OKBLUE = '\033[94m'
OKGREEN = '\033[92m'
WARNING = '\033[93m'
ENDC = '\033[0m'
sys.tracebacklimit=0
def create_service_test():
		INPUT_IMAGE='registry.gitlab.com/jannini/dyslex_ml:latest'
		COMMAND='python main.py '
		INPUT_PARAMETERS=[['3DMH_ny -s -s'],['3DMH_ny -s -s'],['3DMH_ny -s -s'],['3DMH_ny -s -s'],['3DMH_ny -s -s'],['3DMH_ny -s -s']]
		SERVICE_NAME="dyslex_ml_testc"

			#restart_condition='none'
		mount='volume'
		source='docker_test2'
		destination='/results'
		i=1
		for input_param in INPUT_PARAMETERS:
			service_start_command='sudo docker service create --restart-condition=none --with-registry-auth --name '+SERVICE_NAME+str(i)+' --mount type='+mount+',source='+source+',destination='+destination+' '+INPUT_IMAGE +' '+COMMAND+str(input_param[0])
			print('Starting '+OKBLUE+SERVICE_NAME+str(i)+ENDC+' ('+INPUT_IMAGE +' '+COMMAND+str(input_param[0])+')')
			os.system(service_start_command)
			i+=1

def create_service(input_image,command,num_of_threads,mount_folder='/results',service_name=None):
		INPUT_IMAGE=input_image
		COMMAND=command
		NUM_OF_THREADS=num_of_threads
		SERVICE_NAME=service_name

			#restart_condition='none'
		mount='volume'
		source='docker_test2'
		destination=mount_folder
		for i in NUM_OF_THREADS:
			if service_name==None:
				service_start_command='sudo docker service create --restart-condition=none --with-registry-auth --mount type='+mount+',source='+source+',destination='+destination+' '+INPUT_IMAGE +' '+COMMAND+str(i)
				print('Starting '+OKBLUE+'new service'+ENDC+' ('+INPUT_IMAGE +' '+COMMAND+str(i)+')')
			else:
				service_start_command='sudo docker service create --restart-condition=none --with-registry-auth --name '+SERVICE_NAME+str(i)+' --mount type='+mount+',source='+source+',destination='+destination+' '+INPUT_IMAGE +' '+COMMAND+str(i)
				print('Starting '+OKBLUE+SERVICE_NAME+str(i)+ENDC+' ('+INPUT_IMAGE +' '+COMMAND+str(i)+')')
			os.system(service_start_command)


def log_service_terminal():
		services=client.services.list()
		service=services[-1]
		logs=service.logs(stdout=True,follow=True)
		while True:
			next(logs)
			try:
				temp_data=str(logs.gi_frame.f_locals['data'])
				temp_data=temp_data.replace("\\r","\n")
				temp_data=temp_data.replace("b'","")
				temp_data=temp_data.replace("""\\n'""","")

				print(temp_data,end="\n")
				pass
			except:
				pass
			
def cleanup():
		services=client.services.list()
		for service in services:
			task=service.tasks()[0]
			state=task['DesiredState']
			if state=="shutdown":
				print('Removing: '+service.name+"...")
				service.remove()
def cleanup_all():
		services=client.services.list()
		for service in services:
			print('Removing: '+service.name+"...")
			service.remove()


def display_node_status():
		connected_nodes=client.nodes.list()
		total_rams=[]
		available_rams=[]
		availabe_cpus=[]

		for i in range(len(connected_nodes)):
			attrs=connected_nodes[i].attrs
			hostname=attrs['Description']['Hostname']
			host = hostname+'.local'
			port = 22
			if 'fujitsu' in host:
				username = "latlab"
			else:
				username="user"
			password = "mlpass@BIC"
			ssh = paramiko.SSHClient()
			ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
			ssh.connect(host, port, username, password)
			stdin, stdout, stderr = ssh.exec_command("free -m")
			stdin2, stdout2, stderr2 = ssh.exec_command("nproc")
			stdin3, stdout3, stderr3 = ssh.exec_command("""awk '{u=$2+$4; t=$2+$4+$5; if (NR==1){u1=u; t1=t;} else print ($2+$4-u1) * 100 / (t-t1) "%"; }' \
			<(grep 'cpu ' /proc/stat) <(sleep 1;grep 'cpu ' /proc/stat)""")

			lines = stdout.readlines()
			data=lines[1]
			data_formatted=data.split(' ')
			data=list(filter(None,data_formatted))
			number_of_virtual_cores=list(filter(None,stdout2.readlines()))
			total_cpu=int(data[-5])+int(data[-4])+int(data[-3])+int(data[-2])+int(data[-1])
			#cpu_available=int(data[-3])
			cpu_available=round(100-float(stdout3.readlines()[0][:-2]),1)
			total_ram=round(int(data[1])/1000,2)
			ram_available=round(int(data[6])/1000,2)
			total_rams.append(total_ram)
			available_rams.append(ram_available)
			availabe_cpus.append(cpu_available)
			free_cores=math.floor((cpu_available/100)*int(number_of_virtual_cores[0]))
			print('Node: '+hostname)
			print('Available RAM: '+str(ram_available)+' GB of '+str(total_ram)+' GB')
			print('Available CPU: '+str(cpu_available)+' % -> '+str(free_cores)+' out of '+str(number_of_virtual_cores[0])[0] +' cores')
			ssh.close()
		services=client.services.list()
		print('Services: ')
		for service in services:
			task=service.tasks()[0]
			state=task['DesiredState']
			node=client.nodes.get(task['NodeID'])
			node_name=node.attrs['Description']['Hostname']
			if state=="shutdown":
				state="finished"
			if state=="finished":
				print(service.name+' -> node: '+node_name+' -> state: '+OKBLUE+ str(state)+ENDC)
			else:
				print(service.name+' -> node: '+node_name+' -> state: '+OKGREEN+ str(state)+ENDC)

if __name__ == "__main__":


	test=True
	client = docker.from_env()
	if sys.argv[1]=='cleanup':
		cleanup()
	elif sys.argv[1]=='cleanup-all':
		cleanup_all()
	elif sys.argv[1]=='log':
		log_service_terminal()
	elif sys.argv[1]=='run':
		if test:
			create_service_test()
		else:
			if sys.argv[6]==None:
				create_service(sys.argv[2],sys.argv[3],sys.argv[4],sys.argv[5])
			else:
				create_service(sys.argv[2],sys.argv[3],sys.argv[4],sys.argv[5],sys.argv[6])

	elif sys.argv[1]=='status':
		display_node_status()
